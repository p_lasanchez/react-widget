import React from 'react';
import TestRenderer from 'react-test-renderer';
import { Button } from './Button';

describe('<Button />', () => {
  let renderer;

  afterEach(() => {
    renderer.unmount();
    jest.clearAllMocks();
  });

  it('Should render a button', () => {
    const handleClick = jest.fn();
    renderer = TestRenderer.create(
      <Button onClick={handleClick}>Content</Button>
    );
    const instance = renderer.root;
    const button = instance.findByType('button');

    expect(button.props.children).toBe('Content');

    button.props.onClick();
    expect(handleClick).toBeCalled();
  });
});
