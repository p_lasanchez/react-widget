/*
  Button

  props :
  - onClick
  - children

  <button onClick="click()" class="button">
    Texte
  </button>
*/
import React from 'react';
import PropTypes from 'prop-types';
import styles from './button.module.css';

export const Button = ({ children, onClick }) => (
  <button onClick={onClick} className={styles.button}>
    {children}
  </button>
);
Button.propTypes = {
  onClick: PropTypes.func.isRequired,
  children: PropTypes.string.isRequired
};
