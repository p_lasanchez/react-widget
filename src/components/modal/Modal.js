/*
  Modal

  props :
  - children
  - onClose
  - onValidate
  - buttonLabel

  template :
  <div class="modalContainer" onClick="fermer()">
    <div class="modalContent" onClick="stop propagation()">
      <div>Contenu</div>

      <div class="buttonContainer">
        <Button onClick="valider()">Texte bouton</Button>
      </div>
    </div>
  </div>
*/
import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import styles from './modal.module.css';
import { Button } from '..';

export const Modal = ({ children, onClose, onValidate, buttonLabel }) => {
  const handleClickInside = useCallback(event => {
    event.stopPropagation();
    event.preventDefault();
  }, []);

  return (
    <div className={styles.modalContainer} onClick={onClose}>
      <div className={styles.modalContent} onClick={handleClickInside}>
        <div>{children}</div>

        <div className={styles.buttonContainer}>
          <Button onClick={onValidate}>{buttonLabel}</Button>
        </div>
      </div>
    </div>
  );
};

Modal.propTypes = {
  children: PropTypes.node.isRequired,
  onClose: PropTypes.func.isRequired,
  onValidate: PropTypes.func.isRequired,
  buttonLabel: PropTypes.string.isRequired
};
