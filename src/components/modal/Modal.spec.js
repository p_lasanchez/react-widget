import React from 'react';
import TestRenderer from 'react-test-renderer';
import { Modal } from './Modal';
import { Button } from '../button';
import styles from './modal.module.css';

describe('<Modal />', () => {
  let renderer;

  afterEach(() => {
    renderer.unmount();
    jest.clearAllMocks();
  });

  it('Should render a Modal', () => {
    const handleClose = jest.fn();
    const handleValidate = jest.fn();
    const event = {
      stopPropagation: jest.fn(),
      preventDefault: jest.fn()
    };
    renderer = TestRenderer.create(
      <Modal
        onClose={handleClose}
        onValidate={handleValidate}
        buttonLabel="Hello">
        <div testID="id">Contenu</div>
      </Modal>
    );

    const instance = renderer.root;

    const parent = instance.findByProps({ className: styles.modalContainer });
    parent.props.onClick();
    expect(handleClose).toBeCalled();

    const inside = instance.findByProps({ className: styles.modalContent });
    inside.props.onClick(event);
    expect(handleClose).toBeCalledTimes(1);
    expect(event.stopPropagation).toBeCalled();
    expect(event.preventDefault).toBeCalled();

    const child = instance.findByProps({ testID: 'id' });
    expect(child).toBeDefined();

    const button = instance.findByType(Button);
    button.props.onClick();
    expect(handleValidate).toBeCalled();
  });
});
