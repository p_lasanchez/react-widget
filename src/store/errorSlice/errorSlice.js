/*
  errorSlice

  state :
  - errorMessage

  actions :
  - setErrorMessage
  - resetErrorMessage
*/
import { createAction, createSlice } from '@reduxjs/toolkit';

const initialState = {
  errorMessage: ''
};

export const errorSlice = createSlice({
  name: 'errorSlice',
  initialState,
  reducers: {
    setErrorMessage: (state, action) => {
      state.errorMessage = action.payload;
    },
    resetErrorMessage: state => {
      state.errorMessage = '';
    }
  }
});

export const setErrorMessage = createAction('errorSlice/setErrorMessage');
export const resetErrorMessage = createAction('errorSlice/resetErrorMessage');
