/*
  store

  configurer store avec reducers :
  - optionSlice
  - errorSlice
*/
import { configureStore } from '@reduxjs/toolkit';
import { optionSlice } from './optionSlice';
import { errorSlice } from './errorSlice';

export const store = configureStore({
  reducer: {
    optionSlice: optionSlice.reducer,
    errorSlice: errorSlice.reducer
  }
});
