/*
  fetchOption

  service thunk

  utiliser axios get('http://localhost:3333/options')

  utiliser try/catch
  
  retourne response.data ou Promise.reject(error)
*/
import axios from 'axios';
import { createAsyncThunk } from '@reduxjs/toolkit';

export const fetchOption = createAsyncThunk(
  'optionSlice/fetchOptionStatus',
  async () => {
    try {
      const response = await axios.get('http://localhost:3333/options');
      return response.data;
    } catch (error) {
      return Promise.reject(error);
    }
  }
);
