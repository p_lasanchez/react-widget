/*
  optionSlice

  utiliser service fetchOption
  
  state :
  - optionValue (tableau d'options)
  - errorMessage

  reducers : aucun

  extraReducers : fullfilled / rejected
*/
import { createSlice } from '@reduxjs/toolkit';
import { fetchOption } from './fetchOption';

const initialState = {
  optionValue: [],
  errorMessage: ''
};

export const optionSlice = createSlice({
  name: 'optionSlice',
  initialState,
  reducers: {},
  extraReducers: {
    [fetchOption.fulfilled]: (state, action) => {
      state.optionValue = action.payload;
    },
    [fetchOption.rejected]: (state, action) => {
      state.errorMessage = action.error.message;
    }
  }
});
