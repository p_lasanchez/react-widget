import React from 'react';
import PropTypes from 'prop-types';
import { Horloge, Chrono } from '..';

export const Target = ({ selectedElement }) => {
  const targets = {
    horloge: <Horloge />,
    chrono: <Chrono />
  };

  return selectedElement ? targets[selectedElement] : <div />;
};

Target.propTypes = {
  selectedElement: PropTypes.oneOf(['horloge', 'chrono', null])
};
