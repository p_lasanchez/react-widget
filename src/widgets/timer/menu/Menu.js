/*
  Menu

  props :
  - onSelect
  - selected

  template :
  <div class="menuContainer">
    <article
      onClick="click()"
      class="menuItem"
    >

      <icone fill="'darkorange' ou 'white'" />

      <span
        class="menuLabel selected"
      >
        label
      </span>

    </article>
  </div>
*/
import React from 'react';
import PropTypes from 'prop-types';

import Chrono from '../../../assets/icons/chrono.svg';
import Horloge from '../../../assets/icons/horloge.svg';
import styles from '../timer.module.css';
import { MenuIcon } from './MenuIcon';

export const Menu = ({ onSelect, selectedElement }) => {
  const icones = [
    {
      id: 'horloge',
      Icon: Horloge,
      label: 'Horloge',
      isSelected: selectedElement === 'horloge'
    },
    {
      id: 'chrono',
      Icon: Chrono,
      label: 'Chrono',
      isSelected: selectedElement === 'chrono'
    }
  ];

  return (
    <div className={styles.menuContainer}>
      {icones.map(menu => (
        <MenuIcon key={menu.id} menu={menu} onSelect={onSelect} />
      ))}
    </div>
  );
};

Menu.propTypes = {
  onSelect: PropTypes.func.isRequired,
  selectedElement: PropTypes.string
};
