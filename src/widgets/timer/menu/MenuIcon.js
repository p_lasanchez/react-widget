import React, { useCallback } from 'react';
import styles from '../timer.module.css';
import PropTypes from 'prop-types';

export const MenuIcon = ({ menu, onSelect }) => {
  const { id, Icon, isSelected, label } = menu;

  const handleSelect = useCallback(() => {
    onSelect(id);
  }, [id, onSelect]);

  return (
    <article onClick={handleSelect} className={styles.menuItem}>
      <Icon fill={isSelected ? 'darkorange' : 'white'} />
      <span
        className={`${styles.menuLabel} ${isSelected ? styles.selected : ''}`}>
        {label}
      </span>
    </article>
  );
};

MenuIcon.propTypes = {
  menu: PropTypes.shape({
    id: PropTypes.string.isRequired,
    Icon: PropTypes.func.isRequired,
    isSelected: PropTypes.bool.isRequired,
    label: PropTypes.string.isRequired
  }),
  onSelect: PropTypes.func.isRequired
};
