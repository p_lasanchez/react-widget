import React from 'react';
import TestRenderer from 'react-test-renderer';
import { Menu } from './Menu';
import Chrono from '../../../assets/icons/chrono.svg';

describe('Menu', () => {
  let renderer;
  const mockSelect = jest.fn();

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render a default menu', () => {
    renderer = TestRenderer.create(<Menu onSelect={mockSelect} />);
    const root = renderer.root;
    const articles = root.findAllByType('article');

    expect(articles.length).toBe(2);
  });

  it('Should render a selected menu', () => {
    renderer = TestRenderer.create(
      <Menu onSelect={mockSelect} selectedElement="chrono" />
    );
    const root = renderer.root;
    const icon = root.findByType(Chrono);
    expect(icon.props.fill).toBe('darkorange');
  });

  it('Should select a menu', () => {
    renderer = TestRenderer.create(<Menu onSelect={mockSelect} />);
    const root = renderer.root;
    const articles = root.findAllByType('article');

    articles[0].props.onClick();
    expect(mockSelect).toBeCalledWith('horloge');

    articles[1].props.onClick();
    expect(mockSelect).toBeCalledWith('chrono');
  });
});
