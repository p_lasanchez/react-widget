import React from 'react';
import TestRenderer, { act } from 'react-test-renderer';
import { Button } from '../../../components';

import { Chrono } from './Chrono';

describe('<Chrono />', () => {
  let renderer;
  let root;

  beforeEach(() => {
    jest.useFakeTimers();
  });

  afterEach(() => {
    renderer.unmount();
  });

  const setRenderer = () => {
    act(() => {
      renderer = TestRenderer.create(<Chrono />);
    });
    root = renderer.root;
  };

  it('Should render an empty Chrono', () => {
    setRenderer();

    const article = root.findByType('article');
    const button = root.findByType(Button);
    expect(article.props.children).toBe('00 : 00 : 00');
    expect(button.props.children).toBe('Start');
  });

  it('Should start and increment chrono', () => {
    let article;
    let button;

    setRenderer();

    article = root.findByType('article');
    button = root.findByType(Button);

    act(() => {
      button.props.onClick();
      jest.advanceTimersByTime(10);
    });

    expect(article.props.children).toBe('00 : 00 : 01');
    expect(button.props.children).toBe('Stop');

    act(() => {
      jest.advanceTimersByTime(10);
    });
    expect(article.props.children).toBe('00 : 00 : 02');

    act(() => {
      button.props.onClick();
      jest.runAllTimers();
    });
    expect(button.props.children).toBe('Start');
    expect(article.props.children).toBe('00 : 00 : 00');
  });

  it('Should not clear timeout', () => {
    jest.spyOn(global, 'clearTimeout');

    setRenderer();

    act(() => {
      renderer.unmount();
    });
    expect(global.clearTimeout).not.toBeCalled();
  });

  it('Should clear timeout', () => {
    jest.spyOn(global, 'clearTimeout');
    jest.spyOn(global, 'setTimeout').mockReturnValue('done');
    setRenderer();

    const button = root.findByType(Button);

    act(() => {
      button.props.onClick();
    });

    act(() => {
      renderer.unmount();
    });

    expect(global.clearTimeout).toBeCalledWith('done');
  });
});
