/*
  chronoReducer

  utiliser la fonction utils/formatChrono

  state :
  - chrono
  - chronoValue (avec formatChrono)
  - isStarted
  - buttonValue
  
  actions :
  - INCREMENT
  - START
  - STOP
*/
import { formatChrono } from '../utils';

export const initialState = {
  chrono: 0,
  chronoValue: '00 : 00 : 00',
  isStarted: null,
  buttonValue: 'Start'
};

export const chronoReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return {
        ...state,
        chrono: state.chrono + 1,
        chronoValue: formatChrono(state.chrono + 1)
      };

    case 'START':
      return { ...state, isStarted: true, buttonValue: 'Stop' };

    case 'STOP':
      return { ...initialState };

    default:
      return state;
  }
};
