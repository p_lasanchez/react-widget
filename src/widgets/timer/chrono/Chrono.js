/*
  Chrono

  utiliser chronoReducer

  template :
  <div class="content">
    <div class="horlogeContent">
      <section class="chronoContent">
        <article class="chronoValue">valeur chrono</article>

        <Button onClick="click()">start ou stop</Button>
      </section>
    </div>
  </div>
*/
import React, { useCallback, useEffect, useReducer } from 'react';
import styles from '../timer.module.css';
import { chronoReducer, initialState } from './chronoReducer';
import { Button } from '../../../components';

export const Chrono = () => {
  const [state, dispatch] = useReducer(chronoReducer, initialState);

  useEffect(() => {
    let timeout;
    if (state.isStarted) {
      timeout = setTimeout(() => {
        dispatch({ type: 'INCREMENT' });
      }, 10);
    }

    return () => (timeout ? clearTimeout(timeout) : undefined);
  }, [state.chrono, state.isStarted]);

  const handleClick = useCallback(() => {
    dispatch({ type: state.isStarted === true ? 'STOP' : 'START' });
  }, [state.isStarted]);

  return (
    <div className={styles.content}>
      <div className={styles.horlogeContent}>
        <section className={styles.chronoContent}>
          <article className={styles.chronoValue}>{state.chronoValue}</article>

          <Button onClick={handleClick}>{state.buttonValue}</Button>
        </section>
      </div>
    </div>
  );
};
