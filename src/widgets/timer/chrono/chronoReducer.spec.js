import { chronoReducer } from './chronoReducer';

describe('chronoReducer', () => {
  it('Should initialize', () => {
    const result = chronoReducer(undefined, {});

    expect(result).toEqual({
      chrono: 0,
      chronoValue: '00 : 00 : 00',
      isStarted: null,
      buttonValue: 'Start'
    });
  });

  it('Should increment', () => {
    const result = chronoReducer(undefined, { type: 'INCREMENT' });

    expect(result).toEqual({
      chrono: 1,
      chronoValue: '00 : 00 : 01',
      isStarted: null,
      buttonValue: 'Start'
    });
  });

  it('Should start', () => {
    const result = chronoReducer(undefined, { type: 'START' });

    expect(result).toEqual({
      chrono: 0,
      chronoValue: '00 : 00 : 00',
      isStarted: true,
      buttonValue: 'Stop'
    });
  });

  it('Should stop', () => {
    const result = chronoReducer(null, { type: 'STOP' });

    expect(result).toEqual({
      chrono: 0,
      chronoValue: '00 : 00 : 00',
      isStarted: null,
      buttonValue: 'Start'
    });
  });
});
