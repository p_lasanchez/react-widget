import React from 'react';
import TestRenderer, { act } from 'react-test-renderer';
import { TimerWidget } from './TimerWidget';
import { Menu, Horloge, Chrono } from '.';

jest.useFakeTimers();

describe('<TimerWidget />', () => {
  let renderer;

  afterEach(() => {
    renderer.unmount();
    jest.clearAllMocks();
    jest.clearAllTimers();
  });

  it('Should render a TimerWidget', () => {
    act(() => {
      renderer = TestRenderer.create(<TimerWidget />);
    });

    const instance = renderer.root;
    const horloges = instance.findAllByType(Horloge);
    const chronos = instance.findAllByType(Chrono);
    const menu = instance.findByType(Menu);

    expect(horloges.length).toBe(0);
    expect(chronos.length).toBe(0);
    expect(menu.props.selectedElement).toBeNull();

    act(() => {
      menu.props.onSelect('horloge');
    });
    expect(menu.props.selectedElement).toBe('horloge');
    const horloge = instance.findByType(Horloge);
    const noChrono = instance.findAllByType(Chrono);
    expect(horloge).toBeDefined();
    expect(noChrono.length).toBe(0);

    act(() => {
      menu.props.onSelect('chrono');
    });
    expect(menu.props.selectedElement).toBe('chrono');
    const chrono = instance.findByType(Chrono);
    const noHorloge = instance.findAllByType(Horloge);
    expect(chrono).toBeDefined();
    expect(noHorloge.length).toBe(0);
  });
});
