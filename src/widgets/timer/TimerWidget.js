/*
  TimerWidget

  Choose a menu option in order to show the component target (Horloge or Chrono)

  utiliser useState
  
  <div class="main">
    <Horloge /> ou <Chrono />
    <Menu onSelect="selection()" selected="'horloge' ou 'chrono'" />
  </div>
*/
import React, { useState } from 'react';
import { Menu, Target } from '.';
import styles from './timer.module.css';

export const TimerWidget = () => {
  const [selectedElement, setSelectedElement] = useState(null);

  return (
    <div className={styles.main}>
      <Target selectedElement={selectedElement} />
      <Menu onSelect={setSelectedElement} selectedElement={selectedElement} />
    </div>
  );
};
