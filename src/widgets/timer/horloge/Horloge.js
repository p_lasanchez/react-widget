/*
  Horloge

  utiliser useState et useEffect
  
  utiliser setInterval

  utiliser la fonction utils/formatDate

  template :
  <div class="content">
    <article class="horlogeContent">date</article>
  </div>
*/
import React, { useState, useEffect } from 'react';
import styles from '../timer.module.css';
import { formatDate } from '../utils/utils';

export const Horloge = () => {
  const [myDate, setMyDate] = useState(formatDate(new Date()));

  useEffect(() => {
    const interval = setInterval(() => {
      const date = new Date();
      setMyDate(formatDate(date));
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  return (
    <div className={styles.content}>
      <article className={styles.horlogeContent}>{myDate}</article>
    </div>
  );
};
