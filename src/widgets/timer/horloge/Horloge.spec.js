import React from 'react';
import TestRenderer, { act } from 'react-test-renderer';
import { Horloge } from './Horloge';

describe('<Horloge />', () => {
  let renderer;

  beforeEach(() => {
    jest.useFakeTimers('modern');
  });

  afterEach(() => {
    jest.clearAllMocks();
    jest.clearAllTimers();
    renderer.unmount();
  });

  afterAll(() => {
    jest.useRealTimers();
  });

  it('Should render a default date', () => {
    jest.setSystemTime(new Date('2021-02-18 00:00:01'));

    let article;
    act(() => {
      renderer = TestRenderer.create(<Horloge />);
    });
    const root = renderer.root;

    article = root.findByType('article');
    expect(article.props.children).toBe('00 : 00 : 01');

    act(() => {
      jest.advanceTimersByTime(2000);
    });

    article = root.findByType('article');
    expect(article.props.children).toBe('00 : 00 : 03');
  });

  it('Should clear interval on unmount', () => {
    jest.spyOn(global, 'clearInterval');
    jest.spyOn(global, 'setInterval').mockReturnValue('done');
    jest.setSystemTime(new Date('2021-02-18 00:00:01'));
    act(() => {
      renderer = TestRenderer.create(<Horloge />);
    });

    act(() => {
      renderer.unmount();
    });

    expect(global.clearInterval).toBeCalledWith('done');
  });
});
