import { formatDate, formatChrono } from './utils';

describe('utils', () => {
  it('Should format a date', () => {
    const date = new Date('Wed Mar 17 2021 01:43:53 GMT+0100');
    const result = formatDate(date);
    expect(result).toBe('01 : 43 : 53');
  });

  it('Should format a chrono', () => {
    const chrono = 9033;
    const result = formatChrono(chrono);
    expect(result).toBe('01 : 30 : 33');
  });
});
