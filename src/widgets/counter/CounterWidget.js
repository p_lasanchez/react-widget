/*
  CounterWidget
  
  utiliser api context : CounterContext

  utiliser useState

  template :
  <Provider value="?">
    <div class="counterContainer">
      <Action />
      <CounterView />
    </div>
  </Provider>
*/
import React, { useState } from 'react';
import styles from './counter.module.css';
import { Action, CounterView } from './components';
import { CounterContext } from '.';

export const CounterWidget = () => {
  const [counter, setCounter] = useState(0);

  return (
    <CounterContext.Provider value={{ counter, setCounter }}>
      <div className={styles.counterContainer}>
        <Action />
        <CounterView />
      </div>
    </CounterContext.Provider>
  );
};
