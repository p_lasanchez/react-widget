/*
  CounterView

  utiliser contexte

  template :
  <div class="counter">counter</div>
*/
import React, { useContext } from 'react';
import styles from '../counter.module.css';
import { CounterContext } from '..';

export const CounterView = () => {
  const { counter } = useContext(CounterContext);

  return <div className={styles.counter}>{counter}</div>;
};
