/*
  Action

  utiliser contexte

  template : 
  <Button onClick="click()">Ajouter</Button>
*/
import React, { useCallback, useContext } from 'react';
import { Button } from '../../../components/';
import { CounterContext } from '..';

export const Action = () => {
  const { setCounter } = useContext(CounterContext);

  const handleClick = useCallback(() => {
    setCounter(previous => previous + 1);
  }, [setCounter]);

  return <Button onClick={handleClick}>Ajouter</Button>;
};
