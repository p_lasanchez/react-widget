/*
  OptionListWidget

  utiliser redux Provider

  template :
  <Provider store="?">
    <OptionList />
  </Provider>
*/
import React from 'react';
import { store } from '../../store';
import { Provider } from 'react-redux';
import { OptionList } from './list';

export const OptionListWidget = () => {
  return (
    <Provider store={store}>
      <OptionList />
    </Provider>
  );
};
