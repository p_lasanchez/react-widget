import React from 'react';
import TestRenderer from 'react-test-renderer';
import * as redux from 'react-redux';

import { Total } from './Total';

jest.mock('react-redux');

describe('<Total />', () => {
  let renderer;

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render a total', () => {
    const mockState = {
      optionSlice: {
        optionValue: [{ price: 40 }, { price: 60 }]
      }
    };

    jest.spyOn(redux, 'useSelector').mockImplementation(fn => {
      return `${fn(mockState)}`;
    });

    renderer = TestRenderer.create(<Total />);
    const root = renderer.root;
    const el = root.findByType('div');

    expect(el.props.children).toEqual(['100', '€']);
  });
});
