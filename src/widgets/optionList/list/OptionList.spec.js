import React from 'react';
import TestRenderer, { act } from 'react-test-renderer';
import * as redux from 'react-redux';

import { OptionList } from './OptionList';
import { OptionItem } from './OptionItem';

import * as option from '../../../store/optionSlice';
import * as error from '../../../store/errorSlice';

jest.mock('../../../store/optionSlice');
jest.mock('../../../store/errorSlice');
jest.mock('./Total', () => ({
  Total: jest.fn(() => <div />)
}));

describe('<OptionList />', () => {
  let renderer;
  const mockDispatch = jest.fn();

  beforeEach(() => {
    jest.spyOn(redux, 'useDispatch').mockReturnValue(mockDispatch);
  });

  afterEach(() => {
    jest.clearAllMocks();
    renderer.unmount();
  });

  it('Should render an empty list', () => {
    jest.spyOn(option, 'fetchOption');
    jest.spyOn(redux, 'useSelector').mockReturnValue({
      optionValue: [],
      errorMessage: ''
    });

    act(() => {
      renderer = TestRenderer.create(<OptionList />);
    });
    const root = renderer.root;
    const els = root.findAllByType(OptionItem);

    expect(els.length).toBe(0);
    expect(mockDispatch).toBeCalled();
    expect(option.fetchOption).toBeCalled();
  });

  it('Should apply data', () => {
    const optionValue = { label: 'hop', price: 10 };
    const state = { optionSlice: { optionValue: [optionValue] } };
    jest.spyOn(redux, 'useSelector').mockImplementation(fn => {
      return fn(state);
    });

    act(() => {
      renderer = TestRenderer.create(<OptionList />);
    });
    const root = renderer.root;
    const els = root.findByType(OptionItem);

    expect(els.props.option).toEqual(optionValue);
    expect(mockDispatch).not.toBeCalled();
  });

  it('Should set a message', () => {
    jest.spyOn(error, 'setErrorMessage');
    jest.spyOn(redux, 'useSelector').mockReturnValue({
      optionValue: [],
      errorMessage: 'erreur'
    });

    act(() => {
      renderer = TestRenderer.create(<OptionList />);
    });
    expect(error.setErrorMessage).toBeCalledWith('erreur');
  });
});
