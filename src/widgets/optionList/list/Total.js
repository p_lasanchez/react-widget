/*
  Total

  utiliser useSelector redux

  template :
  <div class="total">1000€</div>
*/
import React from 'react';
import { useSelector } from 'react-redux';
import styles from './list.module.css';

export const Total = () => {
  const total = useSelector(state =>
    state.optionSlice.optionValue.reduce((acc, item) => acc + item.price, 0)
  );

  return <div className={styles.total}>{total}€</div>;
};
