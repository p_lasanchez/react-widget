import React from 'react';
import TestRenderer from 'react-test-renderer';
import { OptionItem } from './OptionItem';

describe('<OptionItem />', () => {
  let renderer;

  afterEach(() => {
    renderer.unmount();
    jest.clearAllMocks();
  });

  it('Should render OptionItem', () => {
    const option = {
      label: 'text',
      price: 100
    };

    renderer = TestRenderer.create(<OptionItem option={option} />);

    const instance = renderer.root;
    const spans = instance.findAllByType('span');

    expect(spans[0].props.children).toBe('text');
    expect(spans[1].props.children).toEqual([100, '€']);
  });
});
