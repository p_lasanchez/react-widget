/*
  OptionItem

  props :
  - option { label, price }

  template :
  <article class=article>
    <span>label</span>
    <span>montant €</span>
  </article>
*/
import React from 'react';
import PropTypes from 'prop-types';
import styles from './list.module.css';

export const OptionItem = ({ option }) => {
  return (
    <article className={styles.article}>
      <span>{option.label}</span>
      <span>{option.price}€</span>
    </article>
  );
};

OptionItem.propTypes = {
  option: PropTypes.shape({
    label: PropTypes.string,
    price: PropTypes.number
  }).isRequired
};
