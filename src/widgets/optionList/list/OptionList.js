/*
  OptionList

  Liste des options récupérées par http via redux
  
  utiliser useEffect

  utiliser useSelector
  utiliser useDispatch

  utiliser fetchOption
  utiliser setErrorMessage

  template :
  <section class="optionContainer">
    [
      <OptionItem option="option" />
    ]

    <Total />
  </section>
*/
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchOption } from '../../../store/optionSlice';
import { setErrorMessage } from '../../../store/errorSlice';
import { OptionItem } from './OptionItem';
import { Total } from './Total';
import styles from './list.module.css';

export const OptionList = () => {
  const { optionValue, errorMessage } = useSelector(state => state.optionSlice);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!optionValue.length) {
      dispatch(fetchOption());
    }
  }, [dispatch, optionValue.length]);

  useEffect(() => {
    if (errorMessage) {
      dispatch(setErrorMessage(errorMessage));
    }
  }, [errorMessage, dispatch]);

  return (
    <section className={styles.optionContainer}>
      {optionValue.map(option => (
        <OptionItem key={option.label} option={option} />
      ))}
      <Total />
    </section>
  );
};
