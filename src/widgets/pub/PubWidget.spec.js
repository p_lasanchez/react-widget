import React from 'react';
import TestRenderer from 'react-test-renderer';
import { PubWidget } from './PubWidget';

describe('<PubWidget />', () => {
  let renderer;

  afterEach(() => {
    renderer.unmount();
    jest.clearAllMocks();
  });

  it('Should render PubWidget', () => {
    renderer = TestRenderer.create(<PubWidget />);
    const instance = renderer.root;

    const image = instance.findByType('img');
    expect(image).toBeDefined();
  });
});
