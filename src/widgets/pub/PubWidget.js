/* 
  PubWidget
  ---------

  template :

  <div class="pubContainer">
    <img class="img" src="../../assets/icons/panthere.jpeg" alt="This Is Your Advert" />
    <span>This Is Your Advert</span>
  </div>
 */

import React from 'react';
import styles from './pub.module.css';
import panthere from '../../assets/icons/panthere.jpeg';

export const PubWidget = () => (
  <div className={styles.pubContainer}>
    <img className={styles.img} src={panthere} alt="This Is Your Advert" />
    <span>This Is Your Advert</span>
  </div>
);
