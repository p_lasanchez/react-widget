/*
  HeaderWidget

  Header qui affiche le nom utilisateur (stocké en localStorage)
  Si pas d'utilisateur stocké, ouvrir modale de saisie
  Au click sur bouton ouvrir modale de saisie

  utiliser useState

  utiliser useEffect

  utiliser localStorage.setItem et localStorage.getItem

  utiliser '../../assets/icons/account.svg'

  template :
  <article class="main">
  
    <button onClick="open()" class="button">
      <Account fill="white" />
    </button>

    <span>Welcome Toto</span>

    <HeaderModal
      onClose="close()"
      onValidate="validate()"
      formerName="nom ancien"
    />
    
  </article>
*/
import React, { useCallback, useEffect, useState } from 'react';
import styles from './header.module.css';
import Account from '../../assets/icons/account.svg';
import { HeaderModal } from './HeaderModal';

export const HeaderWidget = () => {
  const [isVisible, setVisible] = useState(false);
  const [name, setName] = useState(null);

  useEffect(() => {
    const savedName = localStorage.getItem('firstName');
    if (savedName) {
      setName(savedName);
    } else {
      setVisible(true);
    }
  }, []);

  const handleOpen = useCallback(() => {
    setVisible(true);
  }, []);

  const handleClose = useCallback(() => {
    setVisible(false);
  }, []);

  const handleValidate = useCallback(input => {
    setName(input);
    localStorage.setItem('firstName', input);
  }, []);

  return (
    <article className={styles.main}>
      <button onClick={handleOpen} className={styles.button}>
        <Account fill="white" />
      </button>

      {name && <span>Welcome {name}</span>}

      {isVisible && (
        <HeaderModal
          onClose={handleClose}
          onValidate={handleValidate}
          formerName={name}
        />
      )}
    </article>
  );
};
