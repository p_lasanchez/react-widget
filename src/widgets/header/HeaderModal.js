/*
  HeaderModal

  props :
  - onClose
  - onValidate
  - formerName

  utiliser useState

  template :
  <Modal onValidate="valider" onClose="close" buttonLabel="Valider">
    <input
      type="text"
      value="nom"
      onChange="change()"
      class="input"
      placeholder="Votre nom"
    />
  </Modal>
*/
import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import styles from './header.module.css';
import { Modal } from '../../components';

export const HeaderModal = ({ onClose, onValidate, formerName }) => {
  const [name, setName] = useState(formerName);

  const handleChange = useCallback(({ target }) => {
    setName(target.value);
  }, []);

  const handleValidate = useCallback(() => {
    onValidate(name);
    onClose();
  }, [name, onClose, onValidate]);

  return (
    <Modal onValidate={handleValidate} onClose={onClose} buttonLabel="Valider">
      <input
        type="text"
        value={name}
        onChange={handleChange}
        className={styles.input}
        placeholder="Votre nom"
      />
    </Modal>
  );
};

HeaderModal.propTypes = {
  onClose: PropTypes.func.isRequired,
  onValidate: PropTypes.func.isRequired,
  formerName: PropTypes.string.isRequired
};
