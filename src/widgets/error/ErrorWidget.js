/*
  ErrorWidget

  Affiche une modale d'erreur

  utiliser redux et Provider

  template :
  <Provider store="?">
    <ErrorModal />
  </Provider>
*/
import React from 'react';
import { store } from '../../store';
import { Provider } from 'react-redux';
import { ErrorModal } from './modal';

export const ErrorWidget = () => {
  return (
    <Provider store={store}>
      <ErrorModal />
    </Provider>
  );
};
