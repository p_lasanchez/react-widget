/*
  useErrorMessage

  utiliser useSelector
  utiliser useDispatch

  return [message d'erreur, fonction reset]
*/
import { useSelector, useDispatch } from 'react-redux';
import { resetErrorMessage } from '../../../store/errorSlice';

export const useErrorMessage = () => {
  const { errorMessage } = useSelector(state => state.errorSlice);
  const dispatch = useDispatch();

  return [errorMessage, () => dispatch(resetErrorMessage())];
};
