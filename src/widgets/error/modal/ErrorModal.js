import React from 'react';
/*
  ErrorModal

  utiliser useErrorMessage

  affichée s'il y a un message d'erreur

  template :
  <Modal
    onClose="reset message"
    onValidate="reset message"
    buttonLabel="Fermer">
    <div class="errorContainer">message</div>
  </Modal>
*/
import { Modal } from '../../../components';
import styles from '../error.module.css';
import { useErrorMessage } from './useErrorMessage';

export const ErrorModal = () => {
  const [errorMessage, resetErrorMessage] = useErrorMessage();

  return errorMessage ? (
    <Modal
      onClose={resetErrorMessage}
      onValidate={resetErrorMessage}
      buttonLabel="Fermer">
      <div className={styles.errorContainer}>{errorMessage}</div>
    </Modal>
  ) : null;
};
