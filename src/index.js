import React from 'react';
import { render } from 'react-dom';
import './css/styles.css';

import { HeaderWidget } from './widgets/header';
import { TimerWidget } from './widgets/timer';
import { PubWidget } from './widgets/pub';
import { OptionListWidget } from './widgets/optionList';
import { ErrorWidget } from './widgets/error';
import { CounterWidget } from './widgets/counter';

// render HeaderWidget to element id 'header-widget'
render(
  React.createElement(HeaderWidget),
  document.getElementById('header-widget')
);

// render TimerWidget to element id 'timer-widget'
render(
  React.createElement(TimerWidget),
  document.getElementById('timer-widget')
);

// render PubWidget to element id 'pub-widget'
render(React.createElement(PubWidget), document.getElementById('pub-widget'));

// render OptionListWidget to element id 'options-widget'
render(
  React.createElement(OptionListWidget),
  document.getElementById('options-widget')
);

// render CounterWidget to element id 'counter-widget'
render(
  React.createElement(CounterWidget),
  document.getElementById('counter-widget')
);

// render ErrorWidget to element id 'error-widget'
render(
  React.createElement(ErrorWidget),
  document.getElementById('error-widget')
);
